package facci.pm.santana.santana.appfacci.contants;

public class ApiConstants {
    // BASE URL
    public static final String BASE_GITHUB_URL = "http://159.65.168.44:3003/";

    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "administrativo/{id}";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/administrativo/{id}";


}
