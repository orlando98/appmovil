package facci.pm.santana.santana.appfacci.adapter;

import java.util.List;


import facci.pm.santana.santana.appfacci.contants.ApiConstants;
import facci.pm.santana.santana.appfacci.model.Administrativo;
import facci.pm.santana.santana.appfacci.service.GitHubService;
import retrofit2.Call;

public class GitHubAdapter extends BaseAdapter implements GitHubService {
    private GitHubService gitHubService;

    public GitHubAdapter() {
        super(ApiConstants.BASE_GITHUB_URL);
        gitHubService = createService(GitHubService.class);
    }

    @Override
    public Call<Administrativo> getOwner(String id) {

        return gitHubService.getOwner(id);
    }

    @Override
    public Call<List<Administrativo>> getOwnerFollowers(String id) {
        return gitHubService.getOwnerFollowers(id);
    }
}
