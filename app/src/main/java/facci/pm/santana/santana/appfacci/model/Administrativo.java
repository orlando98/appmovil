package facci.pm.santana.santana.appfacci.model;
import com.google.gson.annotations.SerializedName;

public class Administrativo {

        @SerializedName("name")
        private String name;

        @SerializedName("apellido")
        private String apellido;

        @SerializedName("cargo")
        private String cargo;

        @SerializedName("imagen")
        private String imagen;


        public String getName() {
            return name;
        }

        public String getLastName() {
            return apellido;
        }

        public String getNivel() {
            return cargo;
        }

        public String getImagen() {
            return imagen;
        }

}
