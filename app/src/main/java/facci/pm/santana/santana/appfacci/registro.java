package facci.pm.santana.santana.appfacci;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import facci.pm.santana.santana.appfacci.adapter.GitHubAdapter;
import facci.pm.santana.santana.appfacci.model.Administrativo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class registro extends AppCompatActivity {
    ArrayList<Administrativo> listaFollowers;


    TextView textViewNombre, textViewApellido, textViewNivel;
    ImageView imageViewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        textViewNombre = findViewById(R.id.txtNombre);
        textViewApellido = findViewById(R.id.txtApellidos);
        textViewNivel = findViewById(R.id.txtCargo);


        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaFollowers = new ArrayList<>();
        RecyclerView recyclerViewFollowers = (AbsListView.RecyclerListener) findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String cedula = getIntent().getStringExtra("cedula");
        mostrarDatosBasicos(id);
        mostrarSeguidores(id);
    }

    private void mostrarDatosBasicos(String loginName){
        GitHubAdapter adapter = new GitHubAdapter();

        Call<Estudiante> call = adapter.getOwner(loginName);

        call.enqueue(new Callback<Estudiante>() {

            @Override
            public void onResponse(Call<Estudiante> call, Response<Estudiante> response) {
                Estudiante owner = response.body();
                textViewNombre.setText(owner.getName().toString());
                textViewApellido.setText(owner.getLastName().toString());
                textViewNivel.setText(owner.getNivel().toString());
                textViewMateriasAprobadas.setText(owner.getMaterias_aprobadas().toString());
                getTextViewMateriasReprobadas.setText(owner.getMaterias_reprobadas().toString());
                Picasso.get().load(owner.getImagen()).into(imageViewProfile);
            }

            @Override
            public void onFailure(Call<Estudiante> call, Throwable t) {

            }
        });


    }

    private void mostrarSeguidores(String cedula){
        GitHubAdapter adapter = new GitHubAdapter();
        Call<List<Administrativo>> call = adapter.getOwnerFollowers(cedula);
        call.enqueue(new Callback<List<Materia>>() {
            @Override
            public void onResponse(Call<List<Administrativo>> call, Response<List<Administrativo>> response) {
                List<Administrativo> lista = response.body();
                for (Administrativo owner: lista) {
                    Log.e("LOGIN", owner.getId());
                    listaFollowers.add(owner);
                }
                AdaptadorFollowers adaptadorFollowers = new AdaptadorFollowers(listaFollowers);
                recyclerViewFollowers.setAdapter(adaptadorFollowers);
            }

            @Override
            public void onFailure(Call<List<Materia>> call, Throwable t) {

            }
        });
    }

}
