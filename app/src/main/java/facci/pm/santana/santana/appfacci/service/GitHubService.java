package facci.pm.santana.santana.appfacci.service;

import java.util.List;

import facci.pm.santana.santana.appfacci.contants.ApiConstants;
import facci.pm.santana.santana.appfacci.model.Administrativo;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.Call;

public interface GitHubService {
    @GET(ApiConstants.GITHUB_USER_ENDPOINT)
    Call<Administrativo> getOwner(@Path("id") String owner);

    @GET(ApiConstants.GITHUB_FOLLOWERS_ENDPOINT)
    Call<List<Administrativo>> getOwnerFollowers(@Path("id") String owner);
}

